import React from 'react';
import { shallow } from 'enzyme';
import PageTitle from './PageTitle';

describe('PageTitle', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<PageTitle title="Title" />);

    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render the "title" property', () => {
    const wrapper = shallow(<PageTitle title="Title" />);

    expect(wrapper.find('.page-title__text').text()).toBe('Title');
  });
});
