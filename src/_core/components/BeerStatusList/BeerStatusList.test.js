import React from 'react';
import { shallow } from 'enzyme';
import BeerStatusList from './BeerStatusList';

describe('BeerStatusList', () => {
  let props;

  beforeEach(() => {
    props = {
      data: [
        {
          id: 1,
          name: 'Acid Dillinger',
          category: 'Pilsner',
          temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
          adequateTemperature: { low: -6, high: -4 },
        },
        {
          id: 2,
          name: 'Humble & Life',
          category: 'IPA',
          temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
          adequateTemperature: { low: -6, high: -5 },
        },
        {
          id: 3,
          name: 'Cowboy Dropsy',
          category: 'Lager',
          temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
          adequateTemperature: { low: -7, high: -4 },
        },
        {
          id: 4,
          name: 'Hysterical & Lizard',
          category: 'Stout',
          temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
          adequateTemperature: { low: -8, high: -6 },
        },
        {
          id: 5,
          name: 'Hootenanny Mystery Box',
          category: 'Wheat Beer',
          temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
          adequateTemperature: { low: -5, high: -3 },
        },
        {
          id: 6,
          name: 'Little Boots',
          category: 'Pale Ale',
          temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
          adequateTemperature: { low: -6, high: -4 },
        },
      ],
    };
  });

  it('should render correctly', () => {
    const wrapper = shallow(<BeerStatusList {...props} />);

    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render the correct number of "BeerStatusCard"', () => {
    const wrapper = shallow(<BeerStatusList {...props} />);

    expect(wrapper.find('BeerStatusCard').length).toBe(props.data.length);
  });
});
