import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Col } from 'reactstrap';

import BeerStatusCard from '../BeerStatusCard/BeerStatusCard';

const BeerStatusList = ({ data }) => {
  return (
    <Fragment>
      {data.map((beerStatusProps, i) => (
        <Col className="align-items-stretch py-3" md={6} lg={4} key={i}>
          <BeerStatusCard {...beerStatusProps} />
        </Col>
      ))}
    </Fragment>
  );
};

BeerStatusList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      category: PropTypes.string.isRequired,
      temperature: PropTypes.number.isRequired,
      adequateTemperature: PropTypes.shape({
        low: PropTypes.number.isRequired,
        high: PropTypes.number.isRequired,
      }).isRequired,
    }),
  ),
};

export default BeerStatusList;
