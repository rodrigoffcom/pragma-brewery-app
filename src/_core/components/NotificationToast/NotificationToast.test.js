import React from 'React';
import { shallow } from 'enzyme';
import NotificationToast from './NotificationToast';

describe('NotificationToast', () => {
  it('should render correctly', () => {
    const wrapper = shallow(
      <NotificationToast content="This is a notification!" createdAt={new Date()} />,
    );

    expect(wrapper.exists()).toBeTruthy();
    expect(wrapper.find('.notification-toast__content').exists()).toBeTruthy();
    expect(wrapper.find('.notification-toast__subtitle').exists()).toBeTruthy();
  });
});
