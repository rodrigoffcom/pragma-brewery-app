import React from 'react';
import PropTypes from 'prop-types';

import { shortDateTimeFormatter } from '../../utils/formatters';

import './NotificationToast.scss';

const NotificationToast = ({ content, createdAt }) => {
  return (
    <span className="notification-toast">
      <p className="notification-toast__content">{content}</p>
      <p className="notification-toast__subtitle">Created at: {shortDateTimeFormatter(createdAt)}</p>
    </span>
  );
};

NotificationToast.propTypes = {
  content: PropTypes.string.isRequired,
};

export default NotificationToast;
