import React from 'react';
import { shallow } from 'enzyme';

import BeerTemperature from './BeerTemperature';

describe('BeerTemperature', () => {
  
  it('should render correctly', () => {
    const wrapper = shallow(<BeerTemperature current={-5.0} low={-6.0} high={-3.0} />);

    expect(wrapper.exists()).toBeTruthy();
  });

  describe('adequate temperature', () => {
    it('should apply the "adequate" class to the elements', () => {
      const wrapper = shallow(<BeerTemperature current={-5.0} low={-6.0} high={-3.0} />);

      expect(wrapper.find('.beer-temperature__text__adequate').exists()).toBeTruthy();
      expect(wrapper.find('.beer-temperature__icon__adequate').exists()).toBeTruthy();
    });

    it('should render the "thumbs-up" icon', () => {
      const wrapper = shallow(<BeerTemperature current={-5.0} low={-6.0} high={-3.0} />);

      expect(wrapper.find('FontAwesomeIcon').props().icon).toBe('thumbs-up');
    });
  });

  describe('low temperature', () => {
    it('should apply the "low" class to the elements', () => {
      const wrapper = shallow(<BeerTemperature current={-7.0} low={-6.0} high={-3.0} />);

      expect(wrapper.find('.beer-temperature__text__low').exists()).toBeTruthy();
      expect(wrapper.find('.beer-temperature__icon__low').exists()).toBeTruthy();
    });

    it('should render the "snowflake" icon', () => {
      const wrapper = shallow(<BeerTemperature current={-7.0} low={-6.0} high={-3.0} />);

      expect(wrapper.find('FontAwesomeIcon').props().icon).toBe('snowflake');
    });
  });

  describe('high temperature', () => {
    it('should apply the "high" class to the elements', () => {
      const wrapper = shallow(<BeerTemperature current={-2.0} low={-6.0} high={-3.0} />);

      expect(wrapper.find('.beer-temperature__text__high').exists()).toBeTruthy();
      expect(wrapper.find('.beer-temperature__icon__high').exists()).toBeTruthy();
    });

    it('should render the "fore" icon', () => {
      const wrapper = shallow(<BeerTemperature current={-2.0} low={-6.0} high={-3.0} />);

      expect(wrapper.find('FontAwesomeIcon').props().icon).toBe('fire');
    });
  });

});
