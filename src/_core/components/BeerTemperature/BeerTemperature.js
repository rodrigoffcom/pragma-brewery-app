import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classnames from 'classnames';

import { temperatureFormatter } from '../../utils/formatters';

import './BeerTemperature.scss';

const BeerTemperature = ({ current, low, high }) => {
  let icon = 'thumbs-up';
  let temperatureClass = 'adequate';
  if (current < low) {
    icon = 'snowflake';
    temperatureClass = 'low';
  }

  if (current > high) {
    icon = 'fire';
    temperatureClass = 'high';
  }

  return (
    <span className={`beer-temperature`}>
      <span
        className={classnames(
          'beer-temperature__text',
          `beer-temperature__text__${temperatureClass}`,
        )}
      >
        {temperatureFormatter(current)}
      </span>
      <FontAwesomeIcon
        className={classnames(
          'beer-temperature__icon',
          `beer-temperature__icon__${temperatureClass}`,
        )}
        icon={icon}
        size="2x"
      />
    </span>
  );
};

BeerTemperature.propTypes = {
  current: PropTypes.number.isRequired,
  low: PropTypes.number.isRequired,
  high: PropTypes.number.isRequired,
}

export default BeerTemperature;
