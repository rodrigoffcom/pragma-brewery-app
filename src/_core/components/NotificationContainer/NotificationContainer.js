import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import NotificationToast from '../NotificationToast/NotificationToast';
import { shiftNotification } from '../../store/notifications/actions';

import './NotificationContainer.scss';

function mapStateToProps({ app: { notifications } }) {
  return {
    notifications,
  };
}

const mapDispatchToProps = {
  shiftNotification,
};

export class NotificationContainer extends Component {
  componentDidUpdate(prevProps) {
    const { notifications: prevNotifications } = prevProps;
    const { notifications, shiftNotification } = this.props;

    if (notifications.length > prevNotifications.length) {
      setTimeout(shiftNotification, 2000);
    }
  }

  render() {
    const { notifications } = this.props;
    return (
      <ReactCSSTransitionGroup
        className="notification-container d-flex flex-column"
        transitionName="fade"
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}
      >
        {notifications.reverse().map((notificationProps, i) => (
          <NotificationToast key={i} {...notificationProps} />
        ))}
      </ReactCSSTransitionGroup>
    );
  }
}

NotificationContainer.propTypes = {
  notifications: PropTypes.arrayOf(
    PropTypes.shape({
      content: PropTypes.string.isRequired,
      createdAt: PropTypes.instanceOf(Date).isRequired,
    }),
  ).isRequired,
  shiftNotification: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationContainer);
