import React from 'React';
import { shallow } from 'enzyme';

import { NotificationContainer } from './NotificationContainer';

describe('NotificationContainer', () => {
  it('should render correctly', () => {
    const wrapper = shallow(
      <NotificationContainer notifications={[]} shiftNotification={jest.fn()} />,
    );

    expect(wrapper.exists()).toBeTruthy();
  });

  it('should render the correct number of "NotificationToast"', () => {
    const notifications = [
      { content: 'This is a notification!', createdAt: new Date() },
      { content: 'This is another notification', createdAt: new Date() },
    ];

    const wrapper = shallow(
      <NotificationContainer notifications={notifications} shiftNotification={jest.fn()} />,
    );

    expect(wrapper.dive().find('NotificationToast').length).toBe(2);
  });

  it('should invoke shiftNotification after 2000ms', () => {
    jest.useFakeTimers();

    const shiftNotification = jest.fn();
    const wrapper = shallow(
      <NotificationContainer notifications={[]} shiftNotification={shiftNotification} />,
    );

    const nextProps = {
      notifications: [{ content: 'This is a notification!', createdAt: new Date() }],
    };
    wrapper.setProps(nextProps);

    jest.advanceTimersByTime(2000);

    expect(shiftNotification).toHaveBeenCalled();
  });
});
