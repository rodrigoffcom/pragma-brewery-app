import React from 'react';
import PropTypes from 'prop-types';

import './PageTitle.scss';

const PageTitle = ({ title }) => {
  return <h1 className="page-title__text">{title}</h1>;
};

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PageTitle;
