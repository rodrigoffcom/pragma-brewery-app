import React from 'react';
import { Container, Navbar, NavbarBrand } from 'reactstrap';

import logo from '../../../_assets/images/logo.svg';

const NavBar = () => {
  return (
    <Navbar color="dark" dark>
      <Container>
        <NavbarBrand href="/" className="d-flex align-items-center mx-auto">
          <img src={logo} alt="Pragma Brewery logo" height={36} className="mr-2" />
          <span>Pragma Brewery</span>
        </NavbarBrand>
      </Container>
    </Navbar>
  );
};

export default NavBar;
