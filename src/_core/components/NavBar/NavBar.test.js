import React from 'react';
import { shallow } from 'enzyme';
import NavBar from './NavBar';

describe('Navbar', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<NavBar />);

    expect(wrapper.exists()).toBeTruthy();
  });
});
