import React from 'react';
import { shallow } from 'enzyme';

import BeerStatusCard from './BeerStatusCard';

describe('BeerStatusCard', () => {
  let props;

  beforeEach(() => {
    props = {
      id: 1,
      name: 'Acid Dillinger',
      category: 'Pilsner',
      temperature: -3,
      adequateTemperature: { low: -6, high: -4 },
      imageUrl: 'https://image',
    };
  });
  
  it('should render correctly', () => {
    const wrapper = shallow(<BeerStatusCard {...props} />);

    expect(wrapper.exists()).toBeTruthy();
  });
});
