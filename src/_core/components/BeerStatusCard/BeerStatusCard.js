import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

import { temperatureFormatter } from '../../utils/formatters';

import './BeerStatusCard.scss';
import BeerTemperature from '../BeerTemperature/BeerTemperature';

const BeerStatusCard = ({ id, name, category, temperature, adequateTemperature }) => {
  return (
    <Card className="beer-status-card">
      <CardBody className="beer-status-card__body">
        <Row>
          <Col className="mb-3">
            <CardTitle className="beer-status-card__body__title">{name}</CardTitle>
            <CardSubtitle className="beer-status-card__body__subtitle">{category}</CardSubtitle>
          </Col>
        </Row>
        <Row className="justify-content-between">
          <Col>
            <span className="beer-status-card__body__adequate-temperature">
              Low {temperatureFormatter(adequateTemperature.low)}
              <br /> High {temperatureFormatter(adequateTemperature.high)}
            </span>
          </Col>
          <Col xs="auto">
            <span className="beer-status-card__body__temperature">
              <BeerTemperature current={temperature} {...adequateTemperature} />
            </span>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

BeerStatusCard.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  temperature: PropTypes.number.isRequired,
  adequateTemperature: PropTypes.shape({
    low: PropTypes.number.isRequired,
    high: PropTypes.number.isRequired,
  }).isRequired,
};
export default BeerStatusCard;
