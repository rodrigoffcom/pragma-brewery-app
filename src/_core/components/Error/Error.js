import React from 'react';
import PropTypes from 'prop-types';

import { Col, Button } from 'reactstrap';

const Error = ({ onClick }) => {
  return (
    <Col className="d-flex flex-column align-items-center">
      <p>An error has occurred while rendering this page.</p>
      <Button className="px-0" color="link" onClick={onClick}>
        Click here to try again
      </Button>
    </Col>
  );
};

Error.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default Error;
