import React from 'react';
import { shallow } from 'enzyme';
import Error from './Error';

describe('Error', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Error onClick={jest.fn()} />);

    expect(wrapper.exists()).toBeTruthy();
  });

  it('should invoke "onClick" on button click', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Error onClick={onClick} />);

    wrapper.find('Button').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
