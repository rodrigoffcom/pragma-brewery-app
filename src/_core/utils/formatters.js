export const temperatureFormatter = temperature =>
  (temperature || 0).toLocaleString(navigator.language, {
    minimumFractionDigits: 1,
    maximumFractionDigits: 1,
  });

export const shortDateTimeFormatter = date =>
  date.toLocaleDateString(navigator.language, {
    day: 'numeric',
    year: 'numeric',
    month: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  });
