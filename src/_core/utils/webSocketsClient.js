import io from 'socket.io-client';

const webSocketsClient = io(process.env.REACT_APP_API_URL);

export default webSocketsClient;
