import configureStore from 'redux-mock-store';

import { dispatchNotification, shiftNotification } from './actions';
import { NOTIFICATIONS_DISPATCH, NOTIFICATIONS_SHIFT } from './actionTypes';

describe('dispatchNotification', () => {
  const mockStore = configureStore();

  it(`should dispatch ${NOTIFICATIONS_DISPATCH}`, () => {
    const store = mockStore();

    const notification = { content: 'Notification content', createdAt: new Date() };
    store.dispatch(dispatchNotification(notification));

    expect(store.getActions()).toEqual([{ type: NOTIFICATIONS_DISPATCH, payload: notification }]);
  });
});

describe('shiftNotification', () => {
  const mockStore = configureStore();

  it(`should dispatch ${NOTIFICATIONS_SHIFT}`, () => {
    const store = mockStore();

    store.dispatch(shiftNotification());

    expect(store.getActions()).toEqual([{ type: NOTIFICATIONS_SHIFT }]);
  });
});
