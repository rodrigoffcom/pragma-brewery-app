import reducer from './reducer';

import { NOTIFICATIONS_DISPATCH, NOTIFICATIONS_SHIFT } from './actionTypes';

describe('reducer', () => {
  it('should return the initial state', () => {
    const state = reducer(undefined, {});

    expect(state).toEqual([]);
  });

  it(`should handle the ${NOTIFICATIONS_DISPATCH} type`, () => {
    const action = {
      type: `${NOTIFICATIONS_DISPATCH}`,
      payload: {
        content: 'This is a notification!',
        createdAt: new Date(),
      },
    };
    const state = reducer(undefined, action);

    expect(state).toEqual([action.payload]);
  });

  it(`should handle the ${NOTIFICATIONS_SHIFT} type`, () => {
    const state = reducer(['This is a notification!'], {
      type: `${NOTIFICATIONS_SHIFT}`,
    });

    expect(state).toEqual([]);
  });
});
