import { NOTIFICATIONS_DISPATCH, NOTIFICATIONS_SHIFT } from './actionTypes';

export function dispatchNotification(notification) {
  return {
    type: NOTIFICATIONS_DISPATCH,
    payload: notification,
  };
}

export function shiftNotification() {
  return {
    type: NOTIFICATIONS_SHIFT,
  };
}
