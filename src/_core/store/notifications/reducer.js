import { NOTIFICATIONS_DISPATCH, NOTIFICATIONS_SHIFT } from './actionTypes';

const initialState = [];

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case NOTIFICATIONS_DISPATCH:
      return [...state, payload];

    case NOTIFICATIONS_SHIFT:
      const notifications = [...state];
      notifications.shift();

      return notifications;

    default:
      return state;
  }
}
