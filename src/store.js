import { createStore, applyMiddleware, combineReducers } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';

import notiticationsReducer from './_core/store/notifications/reducer';
import homePageReducer from './Home/store/reducer';

const combinedReducers = combineReducers({
  app: combineReducers({
    notifications: notiticationsReducer,
  }),
  homePage: homePageReducer,
});

const store = createStore(combinedReducers, applyMiddleware(promiseMiddleware()));

export default store;
