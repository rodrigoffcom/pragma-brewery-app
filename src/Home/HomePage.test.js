import React from 'react';
import { shallow, mount } from 'enzyme';

import { HomePage } from './HomePage';

describe('HomePage', () => {
  let props;

  beforeEach(() => {
    props = {
      isLoading: false,
      hasError: false,
      data: [
        {
          id: 6,
          name: 'Little Boots',
          category: 'Pale Ale',
          temperature: -5.0,
          adequateTemperature: { low: -6, high: -4 },
        },
      ],
      fetchBeers: jest.fn(),
    };
  });

  it('should render correctly', () => {
    const wrapper = shallow(<HomePage {...props} />);

    expect(wrapper.exists()).toBeTruthy();
  });

  it('should invoke "fetchBeers"', () => {
    const fetchBeers = jest.fn();
    shallow(<HomePage {...props} fetchBeers={fetchBeers} />);

    expect(fetchBeers).toHaveBeenCalled();
  });

  it('should match snapshot', () => {
    const wrapper = mount(<HomePage {...props} />);

    expect(wrapper).toMatchSnapshot();
  });
});
