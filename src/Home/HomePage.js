import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';

import { BeerStatusList, Error, Loader, PageTitle } from '../_core/components';

import { fetchBeers } from './store/actions';

import '../fontAwesome';
import './eventsHandler';

function mapStateToProps({ homePage }) {
  return homePage;
}

const mapDispatchToProps = {
  fetchBeers,
};

export class HomePage extends Component {
  constructor(props) {
    super(props);
    this.fetchData();
  }

  fetchData = () => {
    this.props.fetchBeers();
  };

  render() {
    const { isLoading, hasError, data } = this.props;
    return (
      <div>
        <Row>
          <Col>
            <PageTitle title="Beer temperature monitor" />
          </Col>
        </Row>
        <Row>
          {isLoading ? (
            <Loader />
          ) : hasError ? (
            <Error onClick={this.fetchData} />
          ) : (
            <BeerStatusList data={data} />
          )}
        </Row>
      </div>
    );
  }
}

HomePage.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      category: PropTypes.string.isRequired,
      temperature: PropTypes.number.isRequired,
      adequateTemperature: PropTypes.shape({
        low: PropTypes.number.isRequired,
        high: PropTypes.number.isRequired,
      }).isRequired,
    }),
  ),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
