import webSocketsClient from '../_core/utils/webSocketsClient';

import { handleBeerPatched } from './store/actions';

webSocketsClient.on('beers patched', data => {
  handleBeerPatched(data);
});
