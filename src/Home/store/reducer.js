import { PENDING, FULFILLED, REJECTED } from 'redux-promise-middleware';

import { FETCH_BEERS, UPDATE_BEER } from './actionTypes';

const initialState = {
  isLoading: false,
  hasError: false,
  data: [],
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case `${FETCH_BEERS}_${PENDING}`:
      return {
        ...state,
        isLoading: true,
        hasError: false,
        data: [],
      };

    case `${FETCH_BEERS}_${FULFILLED}`:
      return {
        ...state,
        isLoading: false,
        hasError: false,
        data: payload.data,
      };

    case `${FETCH_BEERS}_${REJECTED}`:
      return {
        ...state,
        isLoading: false,
        hasError: true,
        data: [],
      };

    case UPDATE_BEER:
      return {
        ...state,
        data: state.data.map(d => (d.id !== payload.id ? d : payload)),
      };

    default:
      return state;
  }
}
