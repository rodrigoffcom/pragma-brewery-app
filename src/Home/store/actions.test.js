import moxios from 'moxios';
import configureStore from 'redux-mock-store';
import promiseMiddleware from 'redux-promise-middleware';

import httpClient from '../../_core/utils/httpClient';

import { fetchBeers, updateBeer, handleBeerPatched } from './actions';
import { UPDATE_BEER } from './actionTypes';

import { dispatchNotification } from '../../_core/store/notifications/actions';
jest.mock('../../_core/store/notifications/actions', () => ({
  dispatchNotification: jest.fn(() => ({ type: 'dispatchNotification' })),
}));

describe('fetchBeers', () => {
  const mockStore = configureStore([promiseMiddleware()]);

  beforeEach(() => {
    moxios.install(httpClient);
  });

  afterEach(() => {
    moxios.uninstall(httpClient);
  });

  it('should send a get request to "/beers"', () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
      });
    });

    const store = mockStore();

    return store.dispatch(fetchBeers()).then(() => {
      const request = moxios.requests.mostRecent();

      expect(request.url).toContain('/beers');
      expect(request.config.method).toBe('get');
    });
  });
});

describe('updateBeer', () => {
  const mockStore = configureStore();

  it(`should dispatch ${UPDATE_BEER}`, () => {
    const store = mockStore();

    const data = {
      id: 6,
      name: 'Little Boots',
      category: 'Pale Ale',
      temperature: -5.0,
      adequateTemperature: { low: -6, high: -4 },
    };
    store.dispatch(updateBeer(data));

    expect(store.getActions()).toEqual([{ type: UPDATE_BEER, payload: data }]);
  });
});

describe('handleBeerPatched', () => {
  const mockStore = configureStore();

  it(`should dispatch ${UPDATE_BEER}`, () => {
    const store = mockStore();

    const data = {
      id: 6,
      name: 'Little Boots',
      category: 'Pale Ale',
      temperature: -5.0,
      adequateTemperature: { low: -6, high: -4 },
    };
    handleBeerPatched(data, store);

    expect(store.getActions()).toContainEqual({ type: UPDATE_BEER, payload: data });
  });

  it(`should invoke "dispatchNotification"`, () => {
    const store = mockStore();

    const data = {
      id: 6,
      name: 'Little Boots',
      category: 'Pale Ale',
      temperature: -8.0,
      adequateTemperature: { low: -6, high: -4 },
    };
    handleBeerPatched(data, store);

    expect(dispatchNotification).toHaveBeenCalled();
  });
});
