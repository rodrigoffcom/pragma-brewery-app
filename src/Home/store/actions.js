import httpClient from '../../_core/utils/httpClient';
import store from '../../store';

import { dispatchNotification } from '../../_core/store/notifications/actions';

import { FETCH_BEERS, UPDATE_BEER } from './actionTypes';

export function fetchBeers() {
  return {
    type: FETCH_BEERS,
    payload: httpClient.get('/beers'),
  };
}
export function updateBeer(data) {
  return {
    type: UPDATE_BEER,
    payload: data,
  };
}

export function handleBeerPatched(data, applicationStore = store) {
  applicationStore.dispatch(updateBeer(data));

  const {
    name,
    category,
    temperature,
    adequateTemperature: { low, high },
  } = data;
  if (temperature < low || temperature > high) {
    applicationStore.dispatch(
      dispatchNotification({
        content: `${name} (${category}) is outside the temperature threshold. It is now at ${temperature} degrees.`,
        createdAt: new Date(),
      }),
    );
  }
}
