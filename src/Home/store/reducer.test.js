import reducer from './reducer';

import { PENDING, FULFILLED, REJECTED } from 'redux-promise-middleware';

import { FETCH_BEERS } from './actionTypes';

describe('reducer', () => {
  it('should return the initial state', () => {
    const state = reducer(undefined, {});

    expect(state).toEqual({ isLoading: false, hasError: false, data: [] });
  });

  it(`should handle the ${FETCH_BEERS}_${PENDING} type`, () => {
    const state = reducer(
      {
        isLoading: false,
        hasError: true,
        data: [
          {
            id: 6,
            name: 'Little Boots',
            category: 'Pale Ale',
            temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
            adequateTemperature: { low: -6, high: -4 },
          },
        ],
      },
      { type: `${FETCH_BEERS}_${PENDING}` },
    );

    expect(state).toEqual({ isLoading: true, hasError: false, data: [] });
  });

  it(`should handle the ${FETCH_BEERS}_${FULFILLED} type`, () => {
    const data = [
      {
        id: 6,
        name: 'Little Boots',
        category: 'Pale Ale',
        temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
        adequateTemperature: { low: -6, high: -4 },
      },
    ];

    const state = reducer(
      {
        isLoading: true,
        hasError: true,
        data: [],
      },
      {
        type: `${FETCH_BEERS}_${FULFILLED}`,
        payload: {
          data: data,
        },
      },
    );

    expect(state).toEqual({ isLoading: false, hasError: false, data: data });
  });

  it(`should handle the ${FETCH_BEERS}_${REJECTED} type`, () => {
    const state = reducer(
      {
        isLoading: true,
        hasError: false,
        data: [
          {
            id: 6,
            name: 'Little Boots',
            category: 'Pale Ale',
            temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
            adequateTemperature: { low: -6, high: -4 },
          },
        ],
      },
      { type: `${FETCH_BEERS}_${REJECTED}` },
    );

    expect(state).toEqual({ isLoading: false, hasError: true, data: [] });
  });
});
