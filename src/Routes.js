import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import { HomePage } from './Home';

const Routes = () => {
  return (
    <Fragment>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="beer/:id" component={() => null} />
      </Switch>
    </Fragment>
  );
};

export default Routes;
