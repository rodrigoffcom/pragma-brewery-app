import { library } from '@fortawesome/fontawesome-svg-core'
import {  faThumbsUp, faFire, faSnowflake } from '@fortawesome/free-solid-svg-icons'

library.add(faThumbsUp, faFire, faSnowflake);