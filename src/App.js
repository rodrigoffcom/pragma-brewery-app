import React, { Component } from 'react';

import { Provider } from 'react-redux';
import store from './store';

import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './Routes';

import { Container } from 'reactstrap';
import { NavBar, NotificationContainer } from './_core/components';

import './fontAwesome';
import './_assets/styles/_styles.scss';
import './App.scss';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <NotificationContainer />
            <header>
              <NavBar />
            </header>

            <main>
              <Container className="py-3">
                <Routes />
              </Container>
            </main>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
